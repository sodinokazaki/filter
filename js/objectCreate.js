/**
 * Created by nolan.docherty on 15/03/2017.
 */


const createObject = (function () {

    // makes a 'constructor' like object with private variables
    const privateValue = function (value) {
        // private value
        let __value = value;

        const public = {
            getValue: () => __value
        };
        // returns a null if there is no value
        let publicProxy = new Proxy(public,{
            get(target, name) {
                return this.isNothing(target[name]()) ? () => null : target[name];
            },
            isNothing(value) {
                return (value === null || value === undefined );
            }
        })


        return Object.assign({}, publicProxy);
    }


// make fresh object that we will use for prototype methods
    const MapProtoChain = function () {
        const proto = {
            map(fn) {
                return this.of(fn(this.getValue()));
            },
            // return a new clean object with the previous map result
            of(x) {
                return MapChain(x)
            }
        }
        return Object.assign({}, proto);
    };

//We use Object.create to make an object with prototype methods
//then call the constructor object binding the scope to the new proto object and
// passing the wanted value
    const MapChain = (__value) => Object.assign(Object.create(MapProtoChain()), privateValue.call(this, __value));

    return {
        MapChain
    };
})();
