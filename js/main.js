/**
 * Created by nolan.docherty on 15/03/2017.
 */

// you'll need to have read about Object.assign, Object.create,
// Currying, pure functions, template strings and functional programing


// Array[object] => Array[String]
// create and array of objects with a template string
// its not reusable so goes here rather than the helperFn.js


// set up the objects
//String => Object


function searchArrayOI() {
    // this can all be squeezed into one line
    // but for ease of reading it has been broken up
    const inputBox = createObject.MapChain("searchTextOI");
    const searchParam = createObject.MapChain("searchParamOI");
    const listContainer = createObject.MapChain("listContainerOI");

    //get the dom values
    const searchText = inputBox.map(pureHelper.getDomObject).map(pureHelper.getDomObjValue).getValue();
    const group = searchParam.map(pureHelper.getDomObject).map(pureHelper.getDomObjValue).getValue();

    // setup the listfilter
    const listFilter = pureHelper.listFilter(group, searchText);
    //create the document list object
    const docList = createObject.MapChain(getDocumentsList());
    // get an html String
    const filteredList = docList.map(listFilter).map(pureHelper.createHtml).map(pureHelper.concatList).getValue();
    // insert it to the div on the page
    const insertFilteredListToDom = pureHelper.insertToDom(filteredList);

    listContainer.map(pureHelper.getDomObject).map(insertFilteredListToDom);
}


function searchArrayCF() {

    const compose = (...fn) => data => fn.reduceRight((value, func) => func(value), data);

    const getDomValue = compose(pureHelper.getDomObjValue, pureHelper.getDomObject);

    const createHtmlString = compose(pureHelper.concatList, pureHelper.createHtml, pureHelper.listFilter(getDomValue("searchParamCF"), getDomValue("searchTextCF")));
    // get an html String
    // insert it to the div on the page

    const addListTo = compose(pureHelper.insertToDom(createHtmlString(getDocumentsList())), pureHelper.getDomObject);

    addListTo("listContainerCF");
}


function searchArrayPointFreeCF() {
    // a bonus? version of the filter function up to chapter 8 "Releasing the value"
    // if you have read and understood the other two functions have a go at this one whilst rereading the book.

    const safeHead = xs => createObject.MapChain(xs);
    const compose = (...fn) => data => fn.reduceRight((value, func) => func(value), data);
    const map = fn => functor => functor.map(fn);
    //using this means that you dont need to use getValue()
    //  maybe :: b -> (a -> b) -> Maybe a -> b
    const maybe = (x, f) => (m) => {
        return !m.getValue() ? x : f(m.getValue());
    };


    const inputValue = compose(maybe(null, pureHelper.getDomObjValue), map(pureHelper.getDomObject), safeHead);
    // is the same as
    const inputValueBk = compose(maybe(null, compose(pureHelper.getDomObjValue, pureHelper.getDomObject)), safeHead);

    // setup the listfilter

    const fliterListConvertToHtml = compose(maybe(null, pureHelper.concatList), map(pureHelper.createHtml), map(pureHelper.listFilter(inputValue("searchParamPointFreeCF"), inputValue("searchTextPointFreeCF"))), safeHead);
    //is the same as
    const fliterListConvertToHtmlB = compose(maybe(null, compose(pureHelper.concatList, pureHelper.createHtml, pureHelper.listFilter(inputValue("searchParamPointFreeCF"), inputValue("searchTextPointFreeCF")))), safeHead);

    const filteredHtml = fliterListConvertToHtml(getDocumentsList());
    const filteredHtmlB = fliterListConvertToHtmlB(getDocumentsList());

    console.log("filteredHtml == filteredHtmlB", filteredHtml == filteredHtmlB);


    const addListTo = compose(map(pureHelper.insertToDom(filteredHtml)), map(pureHelper.getDomObject), safeHead);
    //is the same as
    const addListToB = compose(map(compose(pureHelper.insertToDom(filteredHtmlB), pureHelper.getDomObject)), safeHead);

    addListTo("listContainerPointFreeCF");
}

