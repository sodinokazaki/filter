/**
 * Created by sod on 23/03/2017.
 */
let getDocumentsList = (function() {
    return documents = [
        {
            "id": "120321",
            "name": "Conditional Fee Agreement",
            "type": "Html",
            "group": "client",
            "createdDate": "2017-02-15T10:40:24.6834126+00:00",
            "modifiedDate": "2017-02-15T10:40:24.6834126+00:00",
            "uploadedDate": "2017-02-15T10:40:24.6834126+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Welcome Information",
            "type": "Html",
            "group": "client",
            "createdDate": "2017-02-15T10:40:24.6834126+00:00",
            "modifiedDate": "2017-02-15T10:40:24.6834126+00:00",
            "uploadedDate": "2017-02-15T10:40:24.6834126+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Terms and Condition",
            "type": "Html",
            "group": "client",
            "createdDate": "2017-02-15T10:40:24.6834126+00:00",
            "modifiedDate": "2017-02-15T10:40:24.6834126+00:00",
            "uploadedDate": "2017-02-15T10:40:24.6834126+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170213153956357_U_79081M_Terms_and_Conditions",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:33.538095+00:00",
            "modifiedDate": "2017-02-15T10:45:33.538095+00:00",
            "uploadedDate": "2017-02-15T10:45:33.538095+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170213153956841_U_79081M_Welcome_Content",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:34.0938685+00:00",
            "modifiedDate": "2017-02-15T10:45:34.0938685+00:00",
            "uploadedDate": "2017-02-15T10:45:34.0938685+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170213153957482_U_79081M_Conditional_Fee_Agreement_-_0%",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:34.5157749+00:00",
            "modifiedDate": "2017-02-15T10:45:34.5157749+00:00",
            "uploadedDate": "2017-02-15T10:45:34.5157749+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170213154554537_U_79081M_Welcome_Information",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:34.7820458+00:00",
            "modifiedDate": "2017-02-15T10:45:34.7820458+00:00",
            "uploadedDate": "2017-02-15T10:45:34.7820458+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170213154554740_U_79081M_Terms_and_Condition",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:36.2802802+00:00",
            "modifiedDate": "2017-02-15T10:45:36.2802802+00:00",
            "uploadedDate": "2017-02-15T10:45:36.2802802+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170213154554912_U_79081M_Conditional_Fee_Agreement",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:36.7269917+00:00",
            "modifiedDate": "2017-02-15T10:45:36.7269917+00:00",
            "uploadedDate": "2017-02-15T10:45:36.7269917+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170215104025676_U_79081M_Terms_and_Conditions",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:37.1413635+00:00",
            "modifiedDate": "2017-02-15T10:45:37.1413635+00:00",
            "uploadedDate": "2017-02-15T10:45:37.1413635+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170215104026161_U_79081M_Welcome_Content",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:37.5525073+00:00",
            "modifiedDate": "2017-02-15T10:45:37.5525073+00:00",
            "uploadedDate": "2017-02-15T10:45:37.5525073+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "20170215104026786_U_79081M_Conditional_Fee_Agreement_-_0%",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-02-15T10:45:37.9373832+00:00",
            "modifiedDate": "2017-02-15T10:45:37.9373832+00:00",
            "uploadedDate": "2017-02-15T10:45:37.9373832+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Jellyfish",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-02T11:06:44.0077168+00:00",
            "modifiedDate": "2017-03-02T11:06:44.0077168+00:00",
            "uploadedDate": "2017-03-02T11:06:44.0077168+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-02T11:10:45.2695487+00:00",
            "modifiedDate": "2017-03-02T11:10:45.2695487+00:00",
            "uploadedDate": "2017-03-02T11:10:45.2695487+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Hydrangeas",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T14:13:11.2057862+00:00",
            "modifiedDate": "2017-03-08T14:13:11.2057862+00:00",
            "uploadedDate": "2017-03-08T14:13:11.2057862+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Chrysanthemum",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:21:06.7957228+00:00",
            "modifiedDate": "2017-03-08T15:21:06.7957228+00:00",
            "uploadedDate": "2017-03-08T15:21:06.7957228+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Desert",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:21:32.4724627+00:00",
            "modifiedDate": "2017-03-08T15:21:32.4724627+00:00",
            "uploadedDate": "2017-03-08T15:21:32.4724627+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:22:20.3734672+00:00",
            "modifiedDate": "2017-03-08T15:22:20.3734672+00:00",
            "uploadedDate": "2017-03-08T15:22:20.3734672+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:23:05.4632468+00:00",
            "modifiedDate": "2017-03-08T15:23:05.4632468+00:00",
            "uploadedDate": "2017-03-08T15:23:05.4632468+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:24:17.147487+00:00",
            "modifiedDate": "2017-03-08T15:24:17.147487+00:00",
            "uploadedDate": "2017-03-08T15:24:17.147487+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:24:43.2584922+00:00",
            "modifiedDate": "2017-03-08T15:24:43.2584922+00:00",
            "uploadedDate": "2017-03-08T15:24:43.2584922+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:25:00.052098+00:00",
            "modifiedDate": "2017-03-08T15:25:00.052098+00:00",
            "uploadedDate": "2017-03-08T15:25:00.052098+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Penguins",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:26:28.5107503+00:00",
            "modifiedDate": "2017-03-08T15:26:28.5107503+00:00",
            "uploadedDate": "2017-03-08T15:26:28.5107503+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Jellyfish",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:26:52.9728448+00:00",
            "modifiedDate": "2017-03-08T15:26:52.9728448+00:00",
            "uploadedDate": "2017-03-08T15:26:52.9728448+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:27:07.6018243+00:00",
            "modifiedDate": "2017-03-08T15:27:07.6018243+00:00",
            "uploadedDate": "2017-03-08T15:27:07.6018243+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:27:50.609042+00:00",
            "modifiedDate": "2017-03-08T15:27:50.609042+00:00",
            "uploadedDate": "2017-03-08T15:27:50.609042+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:28:33.001944+00:00",
            "modifiedDate": "2017-03-08T15:28:33.001944+00:00",
            "uploadedDate": "2017-03-08T15:28:33.001944+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:29:00.0524621+00:00",
            "modifiedDate": "2017-03-08T15:29:00.0524621+00:00",
            "uploadedDate": "2017-03-08T15:29:00.0524621+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:29:18.7039516+00:00",
            "modifiedDate": "2017-03-08T15:29:18.7039516+00:00",
            "uploadedDate": "2017-03-08T15:29:18.7039516+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:30:39.7933953+00:00",
            "modifiedDate": "2017-03-08T15:30:39.7933953+00:00",
            "uploadedDate": "2017-03-08T15:30:39.7933953+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:32:57.4575824+00:00",
            "modifiedDate": "2017-03-08T15:32:57.4575824+00:00",
            "uploadedDate": "2017-03-08T15:32:57.4575824+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:33:33.774487+00:00",
            "modifiedDate": "2017-03-08T15:33:33.774487+00:00",
            "uploadedDate": "2017-03-08T15:33:33.774487+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:34:05.5441865+00:00",
            "modifiedDate": "2017-03-08T15:34:05.5441865+00:00",
            "uploadedDate": "2017-03-08T15:34:05.5441865+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:35:01.6144861+00:00",
            "modifiedDate": "2017-03-08T15:35:01.6144861+00:00",
            "uploadedDate": "2017-03-08T15:35:01.6144861+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:35:25.3060925+00:00",
            "modifiedDate": "2017-03-08T15:35:25.3060925+00:00",
            "uploadedDate": "2017-03-08T15:35:25.3060925+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:36:02.4596451+00:00",
            "modifiedDate": "2017-03-08T15:36:02.4596451+00:00",
            "uploadedDate": "2017-03-08T15:36:02.4596451+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:38:55.1470353+00:00",
            "modifiedDate": "2017-03-08T15:38:55.1470353+00:00",
            "uploadedDate": "2017-03-08T15:38:55.1470353+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:39:48.8735825+00:00",
            "modifiedDate": "2017-03-08T15:39:48.8735825+00:00",
            "uploadedDate": "2017-03-08T15:39:48.8735825+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:40:03.9922443+00:00",
            "modifiedDate": "2017-03-08T15:40:03.9922443+00:00",
            "uploadedDate": "2017-03-08T15:40:03.9922443+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:40:26.3733963+00:00",
            "modifiedDate": "2017-03-08T15:40:26.3733963+00:00",
            "uploadedDate": "2017-03-08T15:40:26.3733963+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Penguins",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-08T15:40:51.3845546+00:00",
            "modifiedDate": "2017-03-08T15:40:51.3845546+00:00",
            "uploadedDate": "2017-03-08T15:40:51.3845546+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Terms and Conditions",
            "type": "Html",
            "group": "client",
            "createdDate": "2017-03-09T08:56:03.4440371+00:00",
            "modifiedDate": "2017-03-09T08:56:03.4440371+00:00",
            "uploadedDate": "2017-03-09T08:56:03.4440371+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:00:07.3058117+00:00",
            "modifiedDate": "2017-03-09T09:00:07.3058117+00:00",
            "uploadedDate": "2017-03-09T09:00:07.3058117+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Penguins",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:00:43.7328396+00:00",
            "modifiedDate": "2017-03-09T09:00:43.7328396+00:00",
            "uploadedDate": "2017-03-09T09:00:43.7328396+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Penguins",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:01:13.2456874+00:00",
            "modifiedDate": "2017-03-09T09:01:13.2456874+00:00",
            "uploadedDate": "2017-03-09T09:01:13.2456874+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:01:36.7895458+00:00",
            "modifiedDate": "2017-03-09T09:01:36.7895458+00:00",
            "uploadedDate": "2017-03-09T09:01:36.7895458+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Tulips",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:01:55.5747886+00:00",
            "modifiedDate": "2017-03-09T09:01:55.5747886+00:00",
            "uploadedDate": "2017-03-09T09:01:55.5747886+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:02:08.7162361+00:00",
            "modifiedDate": "2017-03-09T09:02:08.7162361+00:00",
            "uploadedDate": "2017-03-09T09:02:08.7162361+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:32:25.0856831+00:00",
            "modifiedDate": "2017-03-09T09:32:25.0856831+00:00",
            "uploadedDate": "2017-03-09T09:32:25.0856831+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:33:27.9363843+00:00",
            "modifiedDate": "2017-03-09T09:33:27.9363843+00:00",
            "uploadedDate": "2017-03-09T09:33:27.9363843+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:34:57.5340816+00:00",
            "modifiedDate": "2017-03-09T09:34:57.5340816+00:00",
            "uploadedDate": "2017-03-09T09:34:57.5340816+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Koala",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:36:37.4386046+00:00",
            "modifiedDate": "2017-03-09T09:36:37.4386046+00:00",
            "uploadedDate": "2017-03-09T09:36:37.4386046+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Penguins",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:37:17.4947482+00:00",
            "modifiedDate": "2017-03-09T09:37:17.4947482+00:00",
            "uploadedDate": "2017-03-09T09:37:17.4947482+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Lighthouse",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-09T09:38:06.8459115+00:00",
            "modifiedDate": "2017-03-09T09:38:06.8459115+00:00",
            "uploadedDate": "2017-03-09T09:38:06.8459115+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Acknowledge client post",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-03-09T14:59:05.5384504+00:00",
            "modifiedDate": "2017-03-09T14:59:05.5384504+00:00",
            "uploadedDate": "2017-03-09T14:59:05.5384504+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Acknowledge client post",
            "type": "pdf",
            "group": "Case",
            "createdDate": "2017-03-09T14:59:07.4283905+00:00",
            "modifiedDate": "2017-03-09T14:59:07.4283905+00:00",
            "uploadedDate": "2017-03-09T14:59:07.4283905+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "1489257151623-873359691",
            "type": "jpg",
            "group": "Loss",
            "createdDate": "2017-03-11T18:32:47.9501799+00:00",
            "modifiedDate": "2017-03-11T18:32:47.9501799+00:00",
            "uploadedDate": "2017-03-11T18:32:47.9501799+00:00",
            "isHidden": false
        },
        {
            "id": "120321",
            "name": "Cancellation Form",
            "type": "Html",
            "group": "client",
            "createdDate": "2017-03-14T11:14:40.6026479+00:00",
            "modifiedDate": "2017-03-14T11:14:40.6026479+00:00",
            "uploadedDate": "2017-03-14T11:14:40.6026479+00:00",
            "isHidden": false
        }
    ];
})();
