

/**
 * Created by nolan.docherty on 15/03/2017.
 */

const pureHelper = (function () {
    // pure functions that we will use to map with


//String => Object
// get the dom object
    const getDomObject = id => document.getElementById(id);

//Object => String
//get the dom object value
    const getDomObjValue = obj => obj.value;


// [String] => String
// join an array of strings into a single string
    const concatList = (list) => list.join("");

    const listFilter = (param, value) => listArray => listArray.filter((item) => item[param] === value || item[param].toLowerCase().includes(value.toLowerCase()) ? item : false);


    const insertToDom = child => domObj =>  domObj.innerHTML = child;

    const createHtml = (list) => list.map((item) => {
        return `<div class="documentList">
                    <p>Document Name: ${item.name}</p>
                    <p>Type: ${item.type}</p>
                    <p>created: ${Date(item.createdDate).toLocaleString('en-GB')}</p>
                </div>`
    });

    return {
        getDomObject,
        getDomObjValue,
        concatList,
        listFilter,
        insertToDom,
        createHtml
    };
})();



