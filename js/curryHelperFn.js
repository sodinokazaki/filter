/**
 * Created by nolan.docherty on 15/03/2017.
 */
const curryHelper = (function () {

    // curried function to filter objects from a list
    const listFilter = _.curry(function (param, value, listArray) {
        return listArray.filter((item) => item[param] === value | item[param].toLowerCase().includes(value.toLowerCase())? item : false);
    });

// curried function to insert html string into a dom object
    const insertToDom = _.curry(function (child, domObj) {
        domObj.innerHTML = child;
    });

    return {
        listFilter,
        insertToDom
    };

})();
